import React from "react";
import baretree from "../../media/images/bare-tree.png"

const NoResults = () => {
  return (
    <div className="products">
      <div className="no-results" style={{ marginTop: '10%', height:'90vh'}}>
        <img
          src={baretree}
          alt="Empty Tree"
        />
        <h2>Lo sentimos, no hay productos que coincidan con tu búsqueda!</h2>
        <p>Introduzca una palabra diferente y pruebe.</p>
      </div>
    </div>
  );
};

export default NoResults;

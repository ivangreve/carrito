import React, { Component } from "react";
import Product from "./Product";
import axios from "axios";
import { connect } from 'react-redux';
import updateItemAction from '../../redux/actions/products/updateItem'
import LoadingProducts from "../../loaders/Products";
import NoResults from "./NoResults";
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup";
import InfiniteScroll from 'react-infinite-scroll-component';
import ReactLoading from 'react-loading';

class Products extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor() {
    super();
    this.state = {      
      productsList: [],
      lastPage: 1,
      productsByPage: 46,
      hasMore: false,
      description: "",
      category: "",
      totalItems: 0,
      isLoading: false,
      
    }
  }

  componentDidMount() {
    let description = "";
    this.getProducts(description);
  }

  componentDidUpdate(oldProps) {
    const newProps = this.props;
    if(oldProps.searchTerm !== newProps.searchTerm) {
      this.setState({
        productsList: [],
        lastPage: 1,
        productsByPage: 46,
        hasMore: false,
        description: newProps.searchTerm,
        category: "", 
      })
      this.getProducts(newProps.searchTerm);

    }
    
  }

  setIsLoading(){
    this.setState({
      isLoading: true
    });
  }

  getProducts(description) {
    let url =`http://www.axbot.com.ar/demounilever/api/getArticulos.php?descripcion=${description}&categoria=&startpage=1&limitpage=46`;
    window.scrollTo(0, 0);
    this.setIsLoading();
    axios.get(url).then(response => {
      this.setState({
        productsList: response.data[0].data,
        totalItems: response.data[0].totalItems,
        lastPage: this.state.lastPage + 1,
        isLoading: false,
        hasMore: true
      }, this.createDicProdQuantity(response.data[0].data));
    });
  }
  
  fetchMoreData = () => {
    let { lastPage, productsByPage, description, category } = this.state;
    let url =`http://www.axbot.com.ar/demounilever/api/getArticulos.php?descripcion=${description}&categoria=${category}&startpage=${lastPage}&limitpage=${productsByPage}`;
    this.setIsLoading();
    axios.get(url).then(response => {
      this.setState({
        productsList: this.state.productsList.concat(response.data[0].data),
        totalItems: response.data[0].totalItems,
        lastPage: this.state.lastPage + 1,
        isLoading: false,
        hasMore: true
      },this.createDicProdQuantity(response.data[0].data));
    });
  };

  createDicProdQuantity(lstProducts){
    let that = this;
    lstProducts.forEach(function(product){
      let productKeyNotExist = that.props.productsReducer.dicProd_Store[product.id] === undefined
      if(productKeyNotExist){
        that.props.updateItemAction(product.id, 1);
      }
    });

  }


  formatProductList(){
      let productsData = this.state.productsList
      .map(product => {
        return (          
          <Product
            key={product.id}
            price={product.price}
            name={product.name}
            image={product.image}
            id={product.id}
            addToCart={this.props.addToCart}
            productQuantity={this.props.productQuantity}
            updateQuantity={this.props.updateQuantity}
            openModal={this.props.openModal}
            value={this.props.value}

          />
        )
      }
      );

    
    // Empty and Loading States
    let view;
    if (this.state.isLoading) {
      view = <LoadingProducts />;
    } else if (productsData.length === 0 ) {
      view = <NoResults />;
    } else {
      view = (
        <CSSTransitionGroup
          transitionName="fadeIn"
          transitionEnterTimeout={1}
          transitionLeaveTimeout={1}
          component="div"
          className="products"
        >
          {productsData}
        </CSSTransitionGroup>
      );
    }
    return view;
  }

  renderLoaderOrEnd(){
    let isEndOfList = this.state.productsList.length === this.state.totalItems ;
    let isEmptyList = this.state.productsList.length === 0;

    if(isEmptyList){
      return null;
    }

    if(isEndOfList){
      return  <div style={{marginLeft:"43%", marginTop:"10px", marginBottom:"30px"}} >
                {"Fin de lista"}
              </div>;
    }
    else{
      return <div className="no-result-load"><ReactLoading 
                type={"bubbles"} 
                color={"#077915"} 
              /></div>

    }
  }

  render() {
    return (
      <div className="products-wrapper">
        <InfiniteScroll
          dataLength={this.state.productsList.length}
          next={this.fetchMoreData}
          hasMore={true}
          loader={ this.renderLoaderOrEnd() }
          pullDownToRefreshThreshold={1}
          scrollThreshold={1}
          style={{overflow: "hidden"}}
        >
          { this.formatProductList() }
        </InfiniteScroll>

      </div>
    );
  }
}

//Conecto el store de la applicacion y lo asocio a las props del componente
const mapStateToProps = (state) => {
  return{
    productsReducer: state.productsReducer,
  }
};

// Otra forma más simple!!
const mapDispatchToProps = {
   updateItemAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);

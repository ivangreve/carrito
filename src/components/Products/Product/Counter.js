import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import updateItemAction from '../../../redux/actions/products/updateItem'

class Counter extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      value: this.props.productsReducer.dicProd_Store[this.props.idProduct]
    };

  }

  render() {
    let idProduct = this.props.idProduct;
    return (
      <div className="stepper-input" >
        <a href="# " className="decrement" onClick={this.props.decrement}>
          –
        </a>
        <input
          ref="feedQty"
          type="number"
          className="quantity"
          value={this.props.productsReducer.dicProd_Store[idProduct]}
          onChange={this.props.feed}
        />
        <a href="# " className="increment" onClick={this.props.increment}>
          +
        </a>
      </div>
    );
  }
}

Counter.propTypes = {
  feed: PropTypes.func.isRequired,
  decrement: PropTypes.func.isRequired,
  increment: PropTypes.func.isRequired,
  idProduct: PropTypes.string.isRequired,
};

//Conecto el store de la applicacion y lo asocio a las props del componente
const mapStateToProps = (state) => {
  return{
    productsReducer: state.productsReducer,
  }
};

// Otra forma más simple!!
const mapDispatchToProps = {
   updateItemAction
};

export default connect(mapStateToProps, mapDispatchToProps)(Counter);

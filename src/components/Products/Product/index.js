import React, { Component } from "react";
import Counter from "./Counter";
import { connect } from 'react-redux';
import updateItemAction from '../../../redux/actions/products/updateItem'
import incrementItemAction from '../../../redux/actions/products/incrementItem'
import decrementItemAction from '../../../redux/actions/products/decrementItem'


class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedProduct: {},
      quickViewProdcut: {},
      isAdded: false,
      value: this.props.value
    };

    this.increment = this.increment.bind(this);
    this.decrement = this.decrement.bind(this);
    this.feed = this.feed.bind(this);

  }
  addToCart(image, name, idNombre, price, id, quantity) {
    this.setState(
      {
        selectedProduct: {
          image: image,
          nombreCorto: name,
          name: idNombre,
          price: price,
          id: id,
          quantity: quantity
        }
      },
      function() {
        this.props.addToCart(this.state.selectedProduct);
      }
    );

    let validQuantity = quantity !== "" && quantity !== 0;
    if(validQuantity){
      this.setState({ isAdded: true},
        function() {
          setTimeout(() => {
            this.setState({ isAdded: false, selectedProduct: {} });
          }, 3500);
        }
      );
    }
      
  }
  quickView(image, name, price, id) {
    this.setState(
      {
        quickViewProdcut: {
          image: image,
          name: name,
          price: price,
          id: id
        }
      },
      function() {
        this.props.openModal(this.state.quickViewProdcut);
      }
    );
  }

  increment(e){
    e.preventDefault();
    this.props.incrementItemAction(this.props.id);
  }

  decrement(e){
    e.preventDefault();
    this.props.decrementItemAction(this.props.id);
  }

  feed(e){
    let numberFeeded = e.target.value;
    let isNotANumber = isNaN(numberFeeded);
    if ( isNotANumber || numberFeeded === "0")
      return;

    this.props.updateItemAction(this.props.id, numberFeeded);
  }

  render() {
    let image = this.props.image;
    let name = this.props.name;
    let price = Number(this.props.price).toFixed(2);
    let id = this.props.id;

    return (
      <div className="product">
        <div className="product-image">

        <img
        src= {image}
        alt={this.props.name}
        onClick={this.quickView.bind(
          this,
          image,
          name,
          price,
          id,
          this.props.productsReducer.dicProd_Store[id],
        )}
        onError={(e) => {
            e.target.src = 'http://54.234.100.77:8016/nodisponible.jpg'
           
        }}
        />
      </div>

        <h4 className="product-name">{this.props.name.substr(0, 20)}</h4>
        <p className="product-price">{price}</p>
        
        <Counter
          increment={this.increment}
          decrement={this.decrement}
          feed={this.feed}
          idProduct={id}
        />
        
        <div className="product-action">
          <button
            className={!this.state.isAdded ? "" : "added"}
            type="button"
            onClick={this.addToCart.bind(
              this,
              image,
              name.substr(0, 20),
              `${id}-${name.substr(0, 20)}`,
              parseFloat(price).toFixed(2),
              id,
              this.props.productsReducer.dicProd_Store[id]
            )}
          >
            {!this.state.isAdded ? "AGREGAR" : "✔ AGREGADO"}
          </button>
        </div>  
      </div>
    );
  }
}

//Conecto el store de la applicacion y lo asocio a las props del componente
const mapStateToProps = (state) => {
  return{
    userReducer: state.userReducer,
    productsReducer: state.productsReducer,
  }
};

const mapDispatchToProps = {
   updateItemAction,
   incrementItemAction,
   decrementItemAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Product);
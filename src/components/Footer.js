/* eslint-disable react/jsx-no-target-blank */
import React from "react";

const Footer = () => {
  return (
    <footer>
      <p className="footer-links">
        <a
          href="http://www.axum.com.ar"
          target="_blank"
        >
          Web Site 
        </a>
        <span> / </span>
        <a href="https://es-la.facebook.com/axumvm/" target="_blank">
          Facebook
        </a>
        <span> / </span>
        <a href="https://twitter.com/axumvm" target="_blank">
          Twitter
        </a>
        <span> / </span>
        <a href="mailto:info@axum.com.ar" target="_blank">
          Contacto
        </a>
      </p>
      <p>
        &copy; 2019 <strong>Axum</strong> - Sistemas Inteligentes
      </p>
    </footer>
  );
};

export default Footer;

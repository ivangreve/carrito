import React, { Component } from "react";
import { findDOMNode } from "react-dom";

class QuickView extends Component {
  // eslint-disable-next-line no-useless-constructor
  constructor(props) {
    super(props);
  }
  componentDidMount() {

    document.addEventListener('keyup', (e) => {
      if (e.keyCode === 27) this.props.closeModal();
    });


    document.addEventListener(
      "click",
      this.handleClickOutside.bind(this),
      true
    );
  }

  componentWillUnmount() {
    document.removeEventListener(
      "click",
      this.handleClickOutside.bind(this),
      true
    );
  }

  handleClickOutside(event) {
    const domNode = findDOMNode(this.refs.modal);
    if (!domNode || !domNode.contains(event.target)) {
      this.props.closeModal();
    }
  }

  handleClose() {
    this.props.closeModal();
  }

  render() {
    return (
      <div
        className={
          this.props.openModal ? "modal-wrapper active" : "modal-wrapper"
        }
      >
        <div className="modal" ref="modal">
          <button
            type="button"
            className="close"
            onClick={this.handleClose.bind(this)}
          >
            &times;
          </button>
          <div className="quick-view">
            <div className="quick-view-image">
              <img


                src={this.props.product.image}
                alt={this.props.product.name}
                onError={(e) => {e.target.src = 'http://54.234.100.77:8016/nodisponible.jpg'}}
              />
            </div>
            
            <div className="quick-view-details"> 
                <div className="product-name">{this.props.product.name}</div>
                {/* <div className="product-price">{this.props.product.price}</div> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default QuickView;

import React, { Component } from "react";
import CartScrollBar from "./CartScrollBar";
import EmptyCart from "../../empty-states/EmptyCart";
import CSSTransitionGroup from "react-transition-group/CSSTransitionGroup";
import { findDOMNode } from "react-dom"; 
import ImgClient from "../../media/images/client-green.jpg"
import ImgSearchGreen from "../../media/icons/search-green.png"
import IconBack from "../../media/icons/back.png"
import ImgBag from "../../media/images/bag.png"

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCart: false,
      cart: this.props.cartItems,
      mobileSearch: false
    };
  }
  
  handleCart(e) {
    e.preventDefault();
    this.setState({
      showCart: !this.state.showCart
    });
  }
  handleSubmit(e) {
    e.preventDefault();
  }
  handleMobileSearch(e) {
    e.preventDefault();
    this.setState({
      mobileSearch: true
    });
  }
  handleSearchNav(e) {
    e.preventDefault();
    this.setState(
      {
        mobileSearch: false
      },
      function() {
        this.refs.searchBox.value = "";
        this.props.handleMobileSearch();
      }
    );
  }
  handleClickOutside(e) {
    e.preventDefault();
    const cartNode = findDOMNode(this.refs.cartPreview);
    if (cartNode.classList.contains("active")) {
      if (!cartNode || !cartNode.contains(e.target)) {
        this.setState({
          showCart: false
        });
        e.stopPropagation();
      }
    }
  }
  componentDidMount() {
    document.addEventListener(
      "click",
      this.handleClickOutside.bind(this),
      true
    );
  }
  componentWillUnmount() {
    document.removeEventListener(
      "click",
      this.handleClickOutside.bind(this),
      true
    );
  }

  aceptarcompra(){
    console.log('aceptado')
  }

  render() {
    let cartItems;
    cartItems = this.state.cart.map(product => {
      return (
        <li className="cart-item" key={product.name} onError={(e) => {
          e.target.src = 'http://54.234.100.77:8016/nodisponible.jpg'
         
      }}>
          <img alt="" className="product-image" src={product.image} />
          <div className="product-info">
          
            <p className="product-name">{product.name}</p>
            <p className="product-price">{product.price}</p>
          </div>
          <div className="product-total">
            <p className="quantity">
              {product.quantity} {product.quantity > 1 ? "Cant." : "Cant."}{" "}
            </p>
            <p className="amount">${Number(product.quantity * Number(product.price).toFixed(2)).toFixed(2)}</p>
          </div>
          <a
            className="product-remove"
            href="# "
            onClick={this.props.removeProduct.bind(this, product.id)}
          >
            ×
          </a>
        </li>
      );
    });
    let view;
    if (cartItems.length <= 0) {
      view = <EmptyCart />;
    } else {
      view = (
        <CSSTransitionGroup
          transitionName="fadeIn"
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}
          component="ul"
          className="cart-items"
        >
          {cartItems}
        </CSSTransitionGroup>
      );
    }
    return (
      <header>
        <div className="container">
          <div>

            <img alt="" className="logo" style={{width:"40px", verticalAlign:"middle",marginRight:"5px"}} src={ImgClient}/><span className="clientname">Mariano Gonzalez</span>

            <div className="brand" style={{textAlign:"right"}}>
              <span className="powerby-color">Av. Velez Sarfield 1157</span>
            </div>
          </div>

          <div className="search">
          
           {/* <div>Cant.: <strong>{this.props.totalItems}</strong>  Sub Total: <strong>{this.props.total}</strong></div> */}
            <a
              className="mobile-search"
              href="# "
              onClick={this.handleMobileSearch.bind(this)}
             
            >
           
              <img
                src={ImgSearchGreen}
                alt="search"
              />
            </a>
            <form
              action="#"
              method="get"
              className={
                this.state.mobileSearch ? "search-form active" : "search-form"
              }
            >
              <a
                className="back-button"
                href="# "
                onClick={this.handleSearchNav.bind(this)}
              >
                <img
                  src={IconBack}
                  alt="back"
                />
              </a>
              
              <input
                type="search"
                ref="searchBox"
                placeholder="Buscar producto"
                className="search-keyword"
                onChange={this.props.handleSearch}
                
              />
              <button
                className="search-button"
                type="submit"
                onClick={this.handleSubmit.bind(this)}
              />
            </form>
          </div>

          <div className="cart">
            <div className="cart-info">
              <table>
                <tbody>
                  <tr>
                    <td>Cant. items</td>
                    <td>:</td>
                    <td>
                      <strong>{this.props.totalItems}</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>Sub Total</td>
                    <td>:</td>
                    <td>
                      <strong>{Number(this.props.total).toFixed(2)}</strong>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <a
              className="cart-icon"
              href="# "
              onClick={this.handleCart.bind(this)}
              ref="cartButton"
            >
              <img
                className={this.props.cartBounce ? "tada" : " "}
                src={ImgBag}
                alt="Cart"
              />
              {this.props.totalItems ? (
                <span className="cart-count">{this.props.totalItems}</span>
              ) : (
                ""
              )}
            </a>
            <div
              className={
                this.state.showCart ? "cart-preview active" : "cart-preview"
                
              }
              
              ref="cartPreview"
            >

              <CartScrollBar>{view}</CartScrollBar>
                         
             
              <div className="action-block"> 
              <div className="subcantcompra">Cant: <strong>{this.props.totalItems}</strong>  Sub Total $: <strong>{Number(this.props.total).toFixed(2)}</strong></div>
             
                <button  id="BtnProceder"
                  type="button" onClick={this.aceptarcompra}
                  className={this.state.cart.length > 0 ? " " : "disabled"}
                  
                > 
                  PROCEDER LA COMPRA
              
                </button>
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}

export default Header;

import { createStore, combineReducers } from 'redux';
import userReducer from './reducers/userReducer';
import notificationSnackReducer from './reducers/notificationSnackReducer';
import productsReducer from './reducers/productsReducer';


const reducers = combineReducers({
    userReducer,
    notificationSnackReducer,
    productsReducer,
});
const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

export default store;
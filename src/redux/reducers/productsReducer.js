import { type as updateItemType } from '../actions/products/updateItem';
import { type as incrementItemType } from '../actions/products/incrementItem';
import { type as decrementItemType } from '../actions/products/decrementItem';
import { type as resetItemType } from '../actions/products/resetItem';
import { type as addCartItemType } from '../actions/products/addCartItem';
import { type as removeCartItemType } from '../actions/products/removeCartItem';
import { type as updateCartItemQuantityType } from '../actions/products/updateCartItemQuantity';
import { type as calculateCantItemsType } from '../actions/products/calculateCantItems';
import { type as calculateTotalAmountType } from '../actions/products/calculateTotalAmount';




const defaultState = {
        dicProd_Store: [],
        cartItem_Store: [],
        cartBounce_Store: false,
        totalItems_Store: 0,
        totalAmount_Store: 0,
    };

function reducer(state = defaultState, { type, payload }){
    switch (type) {
        case updateItemType: {
            return updateData(payload.id, payload.value);
        }
        case incrementItemType: {
            return incrementItem(payload.id);
        }
        case decrementItemType: {
            return decrementItem(payload.id);
        }
        case resetItemType: {
            return resetItem(payload.id);
        }
        case addCartItemType: {
            return addCartItem(payload.itemData);
        }
        case removeCartItemType: {
            return removeCartItem(payload.index)
        }
        case updateCartItemQuantityType: {
            return updateCartItemQuantity(payload.index, payload.quantity);
        }
        case calculateCantItemsType: {
            return calculateCantItems();
        }
        case calculateTotalAmountType: {
            return calculateTotalAmount();
        }
        default:
            return state;
    }

    function updateData(id, value){
        let auxDic = state.dicProd_Store;
        auxDic[id] = Number(value);
        return {
            ...state,
            dicProd_Store: auxDic,
        };
    }

    function incrementItem(id){
        state.dicProd_Store[id] = Number(state.dicProd_Store[id]) + 1;
        return { 
            ...state,
            dicProd_Store: state.dicProd_Store,
        };
    }

    function decrementItem(id){
        let actualValue = Number(state.dicProd_Store[id]);
        if (actualValue <= 1) {
            return state;
        } else{
            state.dicProd_Store[id] = actualValue - 1;
            return { 
                ...state,
                dicProd_Store: state.dicProd_Store, 
            };
        }
    }

    function resetItem(id){
        state.dicProd_Store[id] = 1;
        
        return {
            ...state,
            dicProd_Store: state.dicProd_Store,
        };
    }

    function addCartItem(itemData){
        state.cartItem_Store.push(itemData);

        return {
            ...state,
            cartItem_Store: state.cartItem_Store,
        }
    }

    function removeCartItem(index){
        state.cartItem_Store.splice(index, 1);

        return {
            ...state,
            cartItem_Store: state.cartItem_Store,
        }
    }

    function calculateCantItems(){
        let cantItems = state.cartItem_Store.length;
        return {
            ...state,
            totalItems_Store: cantItems,
        }
    }

    function calculateTotalAmount(){
        let totalAmout = 0;

        let cartItems = state.cartItem_Store;
        cartItems.forEach(function(product){
            totalAmout += product.price * parseInt(product.quantity);
        });

        return {
            ...state,
            totalAmount_Store: totalAmout,
        }
    }

    function updateCartItemQuantity(index, quantity){

        state.cartItem_Store[index].quantity = Number(state.cartItem_Store[index].quantity) + Number(quantity);
        return {
            ...state,
            cartItem_Store: state.cartItem_Store,
        }
    }
}

export default reducer;
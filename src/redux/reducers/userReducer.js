import { type as loginCorrectoType } from '../actions/loginCorrecto';



const defaultState = [
    {
        data: []
    }
];

//action => (type,payload)
function reducer(state = defaultState, { type, payload }){

    switch (type) {
        case loginCorrectoType: {
            return [];
        }
        default:
            return state;
    }


}



export default reducer;
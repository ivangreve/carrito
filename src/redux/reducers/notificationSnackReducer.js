import { type as openSnackMessageType } from '../actions/notificationSnack/openSnackMessage';
import { type as closeSnackMessageType } from '../actions/notificationSnack/closeSnackMessage';

const defaultState = [
    {
        message: '',
        isOpen: false,
        autoHideDurationProp: 3000,
        tipoSnack: 'success',
    }
];

function reducer(state = defaultState, { type, payload }){

    switch (type) {
        case openSnackMessageType: {
            return openSnackMessage();
        }
        case closeSnackMessageType: {
            return closeSnackMessage();
        }

        default:
            return state;
    }

    function openSnackMessage(){
        return [
            {
                message: payload.message,
                isOpen: true,
                autoHideDurationProp: payload.autoHideDurationProp,
                tipoSnack: payload.tipoSnack,
            }
        ]
    }

    function closeSnackMessage(){
        var mensajeAnterior = state[0].message;
        var tipoAnterior = state[0].tipoSnack;
        return [
            {
                message: mensajeAnterior,
                isOpen: false,
                autoHideDurationProp: 3000,
                tipoSnack: tipoAnterior,
            }
        ];
        
    }
}

export default reducer;
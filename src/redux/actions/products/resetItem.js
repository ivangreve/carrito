export const type = 'RESET_ITEM';

const resetItem = (id) =>{
    return{
        type: type,
        payload: {
            id,
        },
    };
};


export default resetItem;
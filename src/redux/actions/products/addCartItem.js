export const type = 'ADD_CART_ITEM';

const addCartItem = (itemData) =>{
    return{
        type: type,
        payload: {
            itemData,
        },
    };
};


export default addCartItem;
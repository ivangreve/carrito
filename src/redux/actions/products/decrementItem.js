export const type = 'DECREMENT_ITEM';

const decrementItem = (id) =>{
    return{
        type: type,
        payload: {
            id,
        },
    };
};


export default decrementItem;
export const type = 'UPDATE_CART_iTEM_QUANTITY';

const updateCartItemQuantity = (index, quantity) =>{
    return{
        type: type,
        payload: {
            index,
            quantity,
        },
    };
};


export default updateCartItemQuantity;
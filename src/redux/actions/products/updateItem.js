export const type = 'UPDATE_ITEM';

const updateItem = (id, value) =>{
    return{
        type: type,
        payload: {
            id,
            value,
        },
    };
};


export default updateItem;
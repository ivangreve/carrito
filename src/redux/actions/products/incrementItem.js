export const type = 'INCREMENT_ITEM';

const incrementItem = (id) =>{
    return{
        type: type,
        payload: {
            id,
        },
    };
};


export default incrementItem;
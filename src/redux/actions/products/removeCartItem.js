export const type = 'REMOVE_CART_ITEM';

const removeCartItem = (index) =>{
    return{
        type: type,
        payload: {
            index,
        },
    };
};


export default removeCartItem;
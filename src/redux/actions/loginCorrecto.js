export const type = 'LOGIN_CORRECTO';

const loginCorrecto = (empresa, token, userName) =>{
    return{
        type: type,
        payload: {
            empresa,
            token,
            userName,
        },
    };
};


export default loginCorrecto;
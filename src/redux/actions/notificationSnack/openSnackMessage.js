export const type = 'OPEN_SNACK_MESSAGE';

const openSnackMessage = (message, autoHideDurationProp, tipoSnack) =>{
    return{
        type: type,
        payload: {
            message,
            autoHideDurationProp,
            tipoSnack,
        },
    };
};


export default openSnackMessage;
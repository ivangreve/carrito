export const type = 'CLOSE_SNACK_MESSAGE';

const closeSnackMessage = () =>{
    return{
        type: type,
        payload: {
        },
    };
};

export default closeSnackMessage;
import React, { Component } from "react";
import { connect } from 'react-redux';
import Header from "./components/Header";
import Products from "./components/Products";
import Footer from "./components/Footer";
import QuickView from "./components/QuickView";
import CustomizedSnackbars from './components/CustomizedSnackbars';
import openSnackMessageAction from './redux/actions/notificationSnack/openSnackMessage';
import updateItemAction from './redux/actions/products/updateItem';
import resetItemAction from './redux/actions/products/resetItem';
import addCartItemAction from './redux/actions/products/addCartItem';
import removeCartItemAction from './redux/actions/products/removeCartItem';
import updateCartItemQuantityAction from './redux/actions/products/updateCartItemQuantity';
import calculateTotalAmountAction from './redux/actions/products/calculateTotalAmount';
import calculateCantItemsAction from './redux/actions/products/calculateCantItems';



import "./scss/style.scss";


class App extends Component {
  constructor() {
    super();
    this.state = {
      totalItems: 0,
      totalAmount: 0,
      term: "",
      cartBounce: false,
      quickViewProduct: {},
      modalActive: false,
      cantToAdd: 1,
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.handleMobileSearch = this.handleMobileSearch.bind(this);
    this.handleAddToCart = this.handleAddToCart.bind(this);
    this.checkProduct = this.checkProduct.bind(this);
    this.updateQuantity = this.updateQuantity.bind(this);
    this.handleRemoveProduct = this.handleRemoveProduct.bind(this);
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  // Search by Keyword
  handleSearch(event) {
    this.setState({ term: event.target.value });
  }

  // Mobile Search Reset
  handleMobileSearch() {
    this.setState({ term: "" });
  }

  handleAddToCart(selectedProducts) {
    let productID = selectedProducts.id;
    let productQty = selectedProducts.quantity;

    let invalidQuantity = productQty === "" || productQty === 0;
    if(invalidQuantity){
      let mensajeSnack = "Ingrese cantidad válida!";
      this.props.openSnackMessageAction(mensajeSnack, 2000, 'warning');
      return;
    }

    if (this.checkProduct(productID)) {
      let cartItem = this.props.productsReducer.cartItem_Store;
      let index = cartItem.findIndex(x => x.id === productID);
      this.props.updateCartItemQuantityAction(index, productQty);

    } else {
      this.props.addCartItemAction(selectedProducts);
    }

    this.setState({ cartBounce: true });

    setTimeout(
      function() {
        this.setState({ cartBounce: false });
      }.bind(this),
      1000
    );

    let mensajeSnack = selectedProducts.nombreCorto + "... agregado!";
    this.props.openSnackMessageAction(mensajeSnack, 2000, 'success');

    this.props.resetItemAction(productID);

    this.props.calculateCantItemsAction();
    this.props.calculateTotalAmountAction();

  }

  handleRemoveProduct(id, e) {
    let cartItem = this.props.productsReducer.cartItem_Store;
    let index = cartItem.findIndex(x => x.id === id);

    this.props.removeCartItemAction(index);

    this.props.calculateCantItemsAction();
    this.props.calculateTotalAmountAction();

    e.preventDefault();
  }

  checkProduct(productID) {
    let cart = this.props.productsReducer.cartItem_Store;
    return cart.some(function(item) {
      return item.id === productID;
    });
  }


  updateQuantity(qty) {
    console.log("quantity added...");
    this.setState({
      cantToAdd: qty
    });
  }

  openModal(product) {
    this.setState({
      quickViewProduct: product,
      modalActive: true
    });
  }

  closeModal() {
    this.setState({
      modalActive: false
    });
  }

  render() {

    return (
      <div className="container">
        <Header
          cartBounce={this.state.cartBounce}
          total={this.props.productsReducer.totalAmount_Store} //Llamarlo directamente desde el header
          totalItems={this.props.productsReducer.totalItems_Store}
          cartItems={this.props.productsReducer.cartItem_Store}
          removeProduct={this.handleRemoveProduct}
          handleSearch={this.handleSearch}
          handleMobileSearch={this.handleMobileSearch}
        />
        <Products
          searchTerm={this.state.term}
          addToCart={this.handleAddToCart}
          updateQuantity={this.updateQuantity}
          openModal={this.openModal}
          value={this.state.cantToAdd}
        />
        <Footer />
        <QuickView
          product={this.state.quickViewProduct}
          openModal={this.state.modalActive}
          closeModal={this.closeModal}
        />
        <CustomizedSnackbars/>

      </div>
    );
  }
}


//Conecto el store de la applicacion y lo asocio a las props del componente
const mapStateToProps = (state) => {
  return{
    userReducer: state.userReducer,
    productsReducer: state.productsReducer,
  }
};

// Otra forma más simple!!
const mapDispatchToProps = {
   openSnackMessageAction,
   updateItemAction,
   resetItemAction,
   addCartItemAction,
   updateCartItemQuantityAction,
   removeCartItemAction,
   calculateCantItemsAction,
   calculateTotalAmountAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

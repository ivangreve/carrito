﻿# Axum AxBot-Carrito
Carrito de compras para ventas por whatsapp
## Getting Started
### Prerequisites
- Instalar dependencias npm.
    - Correr: `npm install`

### Running
- En el directorio del proyecto correr:
    - `npm start` (Runs the app in the development mode.)
- Abrir [http://localhost:3000](http://localhost:3000) para visualizar la app.


### Test Runner
- En el directorio del proyecto correr:
    - `npm test`


### Build App
- En el directorio del proyecto correr:
    - `npm run build`
